using System;
using System.Collections.Generic;
using System.Linq;
using PolynomialObject.Exceptions;

namespace PolynomialObject
{
    public sealed class Polynomial
    {
        public Polynomial()
        {
            Members = new List<PolynomialMember>();
        }

        public Polynomial(PolynomialMember member)
        {
            Members = new List<PolynomialMember>();
            if (member.Coefficient != 0)
            {
                Members.Add(member);
            }
        }

        public Polynomial(IEnumerable<PolynomialMember> members)
        {
            this.Members = members.ToList();
        }

        public Polynomial((double degree, double coefficient) member)
        {
            var polynomialMember = new PolynomialMember(member.degree, member.coefficient);
            Members = new List<PolynomialMember>();
            Members.Add(polynomialMember);
        }

        public Polynomial(IEnumerable<(double degree, double coefficient)> members)
        {
            this.Members = new List<PolynomialMember>();
            foreach (var member in members)
            {
                var polynomialMember = new PolynomialMember(member.degree, member.coefficient);
                this.Members.Add(polynomialMember);
            }
        }

        private List<PolynomialMember> Members { get; set; } // mb use dictionary?

        /// <summary>
        /// The amount of not null polynomial members in polynomial 
        /// </summary>
        public int Count
        {
            get
            {
                int _count = default;
                foreach (var item in Members)
                {
                    if (item.Coefficient != 0)
                    {
                        _count++;
                    }
                }
                return _count;
            }
        }

        /// <summary>
        /// The biggest degree of polynomial member in polynomial
        /// </summary>
        public double Degree
        {
            get
            {
                double biggestDegree = default;
                foreach (var item in Members)
                {
                    if (item.Degree > biggestDegree)
                    {
                        biggestDegree = item.Degree;
                    }
                }
                return biggestDegree;
            }
        }

        /// <summary>
        /// Adds new unique member to polynomial 
        /// </summary>
        /// <param name="member">The member to be added</param>
        /// <exception cref="PolynomialArgumentException">Throws when member to add with such degree already exist in polynomial</exception>
        /// <exception cref="PolynomialArgumentNullException">Throws when trying to member to add is null</exception>
        public void AddMember(PolynomialMember member)
        {
            if (member == null)
            {
                throw new PolynomialArgumentNullException();
            }
            else if (this.ContainsMember(member.Degree))
            {
                throw new PolynomialArgumentException();
            }
            else
            {
                Members.Add(member);
            }
        }

        /// <summary>
        /// Adds new unique member to polynomial from tuple
        /// </summary>
        /// <param name="member">The member to be added</param>
        /// <exception cref="PolynomialArgumentException">Throws when member to add with such degree already exist in polynomial</exception>
        public void AddMember((double degree, double coefficient) member)
        {
            PolynomialMember polynomialMember = new PolynomialMember(member.degree, member.coefficient);
            if (this.ContainsMember(polynomialMember.Degree))
            {
                throw new PolynomialArgumentException();
            }
            else
            {
                Members.Add(polynomialMember);
            }
        }

        /// <summary>
        /// Removes member of specified degree
        /// </summary>
        /// <param name="degree">The degree of member to be deleted</param>
        /// <returns>True if member has been deleted</returns>
        public bool RemoveMember(double degree)
        {
            PolynomialMember polynomialMember = Find(degree);
            if (polynomialMember == null)
            {
                return false;
            }
            else
            {
                int index = Members.IndexOf(polynomialMember);
                Members.RemoveAt(index);
                return true;
            }
        }

        /// <summary>
        /// Searches the polynomial for a method of specified degree
        /// </summary>
        /// <param name="degree">Degree of member</param>
        /// <returns>True if polynomial contains member</returns>
        public bool ContainsMember(double degree)
        {
            PolynomialMember polynomialMember = Find(degree);
            if (polynomialMember == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// Finds member of specified degree
        /// </summary>
        /// <param name="degree">Degree of member</param>
        /// <returns>Returns the found member or null</returns>
        public PolynomialMember Find(double degree)
        {
            foreach (var m in Members)
            {
                if (m.Degree == degree)
                {
                    return m;
                }
            }
            return null;
        }

        /// <summary>
        /// Gets and sets the coefficient of member with provided degree
        /// If there is no null member for searched degree - return 0 for get and add new member for set
        /// </summary>
        /// <param name="degree">The degree of searched member</param>
        /// <returns>Coefficient of found member</returns>
        public double this[double degree]
        {
            get
            {
                PolynomialMember polynomialMember = Find(degree);
                if (polynomialMember == null)
                {
                    return 0;
                }
                return polynomialMember.Coefficient;
            }
            set 
            {
                PolynomialMember polynomialMember = Find(degree);
                if (polynomialMember == null)
                {
                    if (value != 0)
                    {
                        Members.Add(new PolynomialMember(degree, value));
                    }
                }
                else
                {
                    int index = Members.IndexOf(polynomialMember);
                    if (value != 0)
                    {
                        Members[index].Coefficient = value;
                    }
                    else
                    {
                        Members.RemoveAt(index);
                    }
                }
            }
        }

        /// <summary>
        /// Convert polynomial to array of included polynomial members 
        /// </summary>
        /// <returns>Array with not null polynomial members</returns>
        public PolynomialMember[] ToArray()
        {
            return Members.ToArray();
        }

        /// <summary>
        /// Adds two polynomials
        /// </summary>
        /// <param name="a">The first polynomial</param>
        /// <param name="b">The second polynomial</param>
        /// <returns>New polynomial after adding</returns>
        /// <exception cref="PolynomialArgumentNullException">Throws if either of provided polynomials is null</exception>
        public static Polynomial operator +(Polynomial a, Polynomial b)
        {
            if (a == null)
            {
                throw new PolynomialArgumentNullException("a");
            }
            if (b == null)
            {
                throw new PolynomialArgumentNullException("b");
            }

            Polynomial result = new Polynomial(a.ToArray());

            foreach (PolynomialMember member in b.Members)
            {
                if (!a.ContainsMember(member.Degree))
                {
                    result.AddMember(member);
                }
            }

            foreach (PolynomialMember member in a.Members)
            {
                result[member.Degree] = member.Coefficient + b[member.Degree];
            }

            return result;
        }

        /// <summary>
        /// Subtracts two polynomials
        /// </summary>
        /// <param name="a">The first polynomial</param>
        /// <param name="b">The second polynomial</param>
        /// <returns>Returns new polynomial after subtraction</returns>
        /// <exception cref="PolynomialArgumentNullException">Throws if either of provided polynomials is null</exception>
        public static Polynomial operator -(Polynomial a, Polynomial b)
        {
            if (a == null)
            {
                throw new PolynomialArgumentNullException("a");
            }
            if (b == null)
            {
                throw new PolynomialArgumentNullException("b");
            }

            Polynomial result = new Polynomial(a.ToArray());

            foreach (PolynomialMember member in b.Members)
            {
                if (!a.ContainsMember(member.Degree))
                {
                    result.AddMember(new PolynomialMember(member.Degree, 0));
                }
            }

            Polynomial tempPolynomial = new Polynomial(result.ToArray());

            foreach (PolynomialMember member in tempPolynomial.Members)
            {
                if (b.ContainsMember(member.Degree))
                {
                    result[member.Degree] = member.Coefficient - b[member.Degree];
                }
            }

            return result;
        }

        /// <summary>
        /// Multiplies two polynomials
        /// </summary>
        /// <param name="a">The first polynomial</param>
        /// <param name="b">The second polynomial</param>
        /// <returns>Returns new polynomial after multiplication</returns>
        /// <exception cref="PolynomialArgumentNullException">Throws if either of provided polynomials is null</exception>
        public static Polynomial operator *(Polynomial a, Polynomial b)
        {
            if (a == null)
            {
                throw new PolynomialArgumentNullException("a");
            }
            if (b == null)
            {
                throw new PolynomialArgumentNullException("b");
            }

            Polynomial result = new Polynomial();

            foreach (var memberOf1stPolynomial in a.Members)
            {
                foreach (var memberOf2ndPolynomial in b.Members)
                {
                    PolynomialMember tempMember = new PolynomialMember(memberOf1stPolynomial.Degree + memberOf2ndPolynomial.Degree,
                                                                       memberOf1stPolynomial.Coefficient * memberOf2ndPolynomial.Coefficient);
                    if (tempMember.Coefficient != 0)
                    {
                        if (result.ContainsMember(tempMember.Degree))
                        {
                            result[tempMember.Degree] += tempMember.Coefficient;
                        }
                        else
                        {
                            result.AddMember(tempMember);
                        }
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Adds polynomial to polynomial
        /// </summary>
        /// <param name="polynomial">The polynomial to add</param>
        /// <returns>Returns new polynomial after adding</returns>
        /// <exception cref="PolynomialArgumentNullException">Throws if provided polynomial is null</exception>
        public Polynomial Add(Polynomial polynomial)
        {
            if (polynomial == null)
            {
                throw new PolynomialArgumentNullException();
            }

            Polynomial result = new Polynomial(this.ToArray());

            foreach (PolynomialMember member in polynomial.Members)
            {
                if (!this.ContainsMember(member.Degree))
                {
                    result.AddMember(member);
                }
            }

            foreach (PolynomialMember member in this.Members)
            {
                result[member.Degree] = member.Coefficient + polynomial[member.Degree];
            }

            return result;
        }

        /// <summary>
        /// Subtracts polynomial from polynomial
        /// </summary>
        /// <param name="polynomial">The polynomial to subtract</param>
        /// <returns>Returns new polynomial after subtraction</returns>
        /// <exception cref="PolynomialArgumentNullException">Throws if provided polynomial is null</exception>
        public Polynomial Subtraction(Polynomial polynomial)
        {
            if (polynomial == null)
            {
                throw new PolynomialArgumentNullException();
            }

            Polynomial result = new Polynomial(this.ToArray());

            foreach (PolynomialMember member in polynomial.Members)
            {
                if (!this.ContainsMember(member.Degree))
                {
                    result.AddMember(new PolynomialMember(member.Degree, 0));
                }
            }

            Polynomial tempPolynomial = new Polynomial(result.ToArray());

            foreach (PolynomialMember member in tempPolynomial.Members)
            {
                if (polynomial.ContainsMember(member.Degree))
                {
                    result[member.Degree] = member.Coefficient - polynomial[member.Degree];
                }
            }

            return result;
        }

        /// <summary>
        /// Multiplies polynomial with polynomial
        /// </summary>
        /// <param name="polynomial">The polynomial for multiplication </param>
        /// <returns>Returns new polynomial after multiplication</returns>
        /// <exception cref="PolynomialArgumentNullException">Throws if provided polynomial is null</exception>
        public Polynomial Multiply(Polynomial polynomial)
        {
            if (polynomial == null)
            {
                throw new PolynomialArgumentNullException();
            }

            Polynomial result = new Polynomial();

            foreach (var memberOf1stPolynomial in this.Members)
            {
                foreach (var memberOf2ndPolynomial in polynomial.Members)
                {
                    PolynomialMember tempMember = new PolynomialMember(memberOf1stPolynomial.Degree + memberOf2ndPolynomial.Degree,
                                                                       memberOf1stPolynomial.Coefficient * memberOf2ndPolynomial.Coefficient);
                    if (tempMember.Coefficient != 0)
                    {
                        if (result.ContainsMember(tempMember.Degree))
                        {
                            result[tempMember.Degree] += tempMember.Coefficient;
                        }
                        else
                        {
                            result.AddMember(tempMember);
                        }
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Adds polynomial and tuple
        /// </summary>
        /// <param name="a">The polynomial</param>
        /// <param name="b">The tuple</param>
        /// <returns>Returns new polynomial after adding</returns>
        public static Polynomial operator +(Polynomial a, (double degree, double coefficient) b)
        {
            if (a == null)
            {
                throw new PolynomialArgumentNullException("a");
            }

            Polynomial result = new Polynomial(a.ToArray());

            if (b.coefficient != 0)
            {
                var polynomialMember = new PolynomialMember(b.degree, b.coefficient);

                if (!a.ContainsMember(polynomialMember.Degree))
                {
                    result.AddMember(polynomialMember);
                }
                else
                {
                    result[polynomialMember.Degree] = polynomialMember.Coefficient + a[polynomialMember.Degree];
                }
            }

            return result;
        }

        /// <summary>
        /// Subtract polynomial and tuple
        /// </summary>
        /// <param name="a">The polynomial</param>
        /// <param name="b">The tuple</param>
        /// <returns>Returns new polynomial after subtraction</returns>
        public static Polynomial operator -(Polynomial a, (double degree, double coefficient) b)
        {
            if (a == null)
            {
                throw new PolynomialArgumentNullException("a");
            }

            Polynomial result = new Polynomial(a.ToArray());

            var polynomialMember = new PolynomialMember(b.degree, b.coefficient);

            if (!a.ContainsMember(polynomialMember.Degree))
            {
                result.AddMember(new PolynomialMember(polynomialMember.Degree, 0));
            }
            result[polynomialMember.Degree] = a[polynomialMember.Degree] - polynomialMember.Coefficient;

            return result;
        }

        /// <summary>
        /// Multiplies polynomial and tuple
        /// </summary>
        /// <param name="a">The polynomial</param>
        /// <param name="b">The tuple</param>
        /// <returns>Returns new polynomial after multiplication</returns>
        public static Polynomial operator *(Polynomial a, (double degree, double coefficient) b)
        {
            if (a == null)
            {
                throw new PolynomialArgumentNullException("a");
            }

            Polynomial result = new Polynomial();

            var polynomialMember = new PolynomialMember(b.degree, b.coefficient);

            foreach (var member in a.Members)
            {
                PolynomialMember tempMember = new PolynomialMember(member.Degree + polynomialMember.Degree,
                                                                    member.Coefficient * polynomialMember.Coefficient);
                if (tempMember.Coefficient != 0)
                {
                    if (result.ContainsMember(tempMember.Degree))
                    {
                        result[tempMember.Degree] += tempMember.Coefficient;
                    }
                    else
                    {
                        result.AddMember(tempMember);
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Adds tuple to polynomial
        /// </summary>
        /// <param name="member">The tuple to add</param>
        /// <returns>Returns new polynomial after adding</returns>
        public Polynomial Add((double degree, double coefficient) member)
        {
            Polynomial result = new Polynomial(this.ToArray());

            if(member.coefficient != 0)
            {
                var polynomialMember = new PolynomialMember(member.degree, member.coefficient);

                if (!result.ContainsMember(polynomialMember.Degree))
                {
                    result.AddMember(polynomialMember);
                }
                else
                {
                    result[polynomialMember.Degree] = polynomialMember.Coefficient + this[polynomialMember.Degree];
                }
            }

            return result;
        }

        /// <summary>
        /// Subtracts tuple from polynomial
        /// </summary>
        /// <param name="member">The tuple to subtract</param>
        /// <returns>Returns new polynomial after subtraction</returns>
        public Polynomial Subtraction((double degree, double coefficient) member)
        {
            Polynomial result = new Polynomial(this.ToArray());

            var polynomialMember = new PolynomialMember(member.degree, member.coefficient);

            if (!this.ContainsMember(polynomialMember.Degree))
            {
                result.AddMember(new PolynomialMember(polynomialMember.Degree, 0));
            }
            result[polynomialMember.Degree] = this[polynomialMember.Degree] - polynomialMember.Coefficient;

            return result;
        }

        /// <summary>
        /// Multiplies tuple with polynomial
        /// </summary>
        /// <param name="member">The tuple for multiplication </param>
        /// <returns>Returns new polynomial after multiplication</returns>
        public Polynomial Multiply((double degree, double coefficient) member)
        {
            Polynomial result = new Polynomial();

            var polynomialMember = new PolynomialMember(member.degree, member.coefficient);

            foreach (var m in this.Members)
            {
                PolynomialMember tempMember = new PolynomialMember(m.Degree + polynomialMember.Degree,
                                                                   m.Coefficient * polynomialMember.Coefficient);
                if (tempMember.Coefficient != 0)
                {
                    if (result.ContainsMember(tempMember.Degree))
                    {
                        result[tempMember.Degree] += tempMember.Coefficient;
                    }
                    else
                    {
                        result.AddMember(tempMember);
                    }
                }
            }

            return result;
        }
    }
}
